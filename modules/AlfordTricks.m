(* ::Package:: *)

(* A bunch of useful things, all implemented inefficiently 
   M. Alford 1998-2011
*)

BeginPackage["MGALibrary`"];

(* ---- Contents:

General:
 TestZero[x_,accuracy_:10^(-10)]
 DateToString[]
 SetAppend[set_,elem_]

Crude Solutions:
 CrudeFindRoot[fn_,boundlist_,accuracy_:0.001,verbose_:False]
   boundlist is {lwb,upb} with optional additional hard bounds
 CrudeFindMax[fn_,boundlist_,x$accuracy_:0.001,y$accuracy_:0.001,verbose_:False]   boundlist is {lwb,upb} with optional additional hard bounds

Crude Plotting:
 CrudePlot[fn_,range_,npts_:20,plotargs_:(Joined -> True)]
    " fn can return a list of numbers "
 CumulativeCrudePlot[fn_,range_,npts_:20] " cumulates points in CCPList "
 MultiCrudePlot[fn$list_,range_,npts_:20,verbose_:False,
  plotargs_:(Joined -> True)]  " plots a list of fns "

 MultiListPlot[list$of$lists_,plotargs_:(Joined->True)]
   " each list is {{x,y},{x,y}...} "

Matrices:
 Adjoint[matrix_] 
 Trace[matrix_]
 Commutator[a_,b_]
 Anticommutator[a_,b_]
 Orthonormalize[matrix_]
 OuterProduct[mata_,matb_]
 TProd or TensorProduct[mata_,matb_]
 Hackup[matrix_,nr_,nc_]
 UnHackup[matrix_]

Output: 
  CFloat[x_,n_]       CForm string with n digits of precision
  PadFloat[x_,w_,n_]  CForm string with n digits of precision and width w
  RoundFloat[x_,n_]   MathForm float with n digits of precision
  PadString[s_,w_]

---- *)

{CFloat,
RoundFloat,
PadFloat,
PadString,
TestZero,
DateToString,
SetAppend,
CrudeFindRoot,
CrudeFindMax,
CrudePlot,
CCPList,  (* used by CumulativeCrudePlot *)
CumulativeCrudePlot,
MultiListPlot,
Adjoint,
Commutator,
Anticommutator,
OuterProduct,
Orthonormalize,
TensorProduct,TProd,
Hackup,
UnHackup
}


Begin["`Private`"];


CFloat[x_?NumberQ,n_?IntegerQ]:= ToString[CForm[SetPrecision[x,n]]];
(*  CFloat[ Exp[30.0], 5]        1.0686e13 *)
RoundFloat[x_?NumberQ,n_?IntegerQ]:= StringReplace[CFloat[x,n],"e"->"*^"];
(*  RoundFloat[ Exp[30.0], 5]    1.0686*^13 *)

PadFloat[x_?NumberQ,w_?IntegerQ,f_?IntegerQ]:=Module[{str,point$range,n},
 (* C representation of x, with f digits of precision, and
    overall width of w characters *)
 str = ToString[CForm[SetPrecision[x,f]]];
 point$range = StringPosition[str,"."];
 If[Length[point$range]>0, (* found a "." somewhere *)
  If[Length[point$range]<=1, (* only found one "." *)
   n = point$range[[1]][[1]];
   If[n==StringLength[str],
   (* if str ends with a ".", we either strip it off (if there are
     already enough sig figs before the point) or add a zero afterwards *)
    If[n-1 >= f,
     str=StringTake[str,n-1] (* alt: strip off the point: StringTake[str,n-1] *)
    ,(*else*)
     str=str<>"0" 
    ]
   ]
  ]
 ];    
 While[StringLength[str]<w, str = " "<>str];
 str
];

PadString[s_?StringQ,w_?IntegerQ,side_:-1] := Module[{str},
 (* string is padded to width w, side=-1 is left padding, 
    side=+1 is right padding, side=0 is centering *)
 str=s;
 If[side<0,
  While[StringLength[str]<w, str = " "<>str];
 ,(*else*)
  If[side>0,
   While[StringLength[str]<w, str = str<>" "];
  ,(*else*)
   While[StringLength[str]<w, 
    If[EvenQ[StringLength[str]],str=" "<>str, str=str<>" "]
   ]
  ]
 ];
 str
];


TestZero[x_,accuracy_:10^(-10)]:=Module[{bool},
 bool = NumberQ[x];
 If[bool,
  bool = Abs[x]<accuracy
 ];
 bool
];

DateToString[]:=Module[{dl,dls},
  (* This is a date string such that alphabetic ordering of the strings
  is the same as temporal ordering of the dates *)
 dl=DateList[];
 dl[[-1]]=Round[dl[[-1]]]; (* round seconds to an integer *)
 dls=Map[ToString[#]&, dl]; (* convert integers to strings *)
 (* left-pad with zero as necessary *)
 Map[ If[StringLength[#]<2, StringJoin["0",#], #]&, dls]
];

SetAppend[set_,elem_,DiffFn_:Subtract,verbose_:False] := Module[
 {i,difference,different},
(* Adds elem to set only if there is not one already there *)
 If[verbose,Print["SetAppend: considering ",elem]];
 different = True;
 i=1;
 While[i<=Length[set] && different,
  difference = DiffFn[elem, set[[i]]];
  different = ! NumberQ[difference];
  If[! different, different = difference!=0];
  i++;
  If[verbose && !different,
   Print["SetAppend: same as elem ",i];
  ];
 ];
 If[different, Return[Append[set,elem]]
 , Return[set] ]
];

CrudeFindRoot[fn_,boundlist_,accuracy_:0.001,verbose_:False]:=
(* Find x such that fn[x]==0,
    eg:  CrudeFindRoot[ Cos, {0,Pi}]   
   M. Alford   June 1998, Feb 2008
   boundlist: {lwb,upb} or {lwb,upb,hardbound} or {lwb,upb,hardbound,hardbound}
   {lwb,upb} need not straddle the root; it shifts them in the
   appropriate direction in search of a zero.
   The hard bounds cannot be crossed: the search
   will only asymptotically approach those values.
*)
Module[{lwb,upb,hard$lwb,hard$upb,Klwb,Kupb,mid,Kmid,step,dirn,last$x},
  (* last$x keeps track of the most recent evaluation of the function *)
  lwb=1.0*Min[boundlist[[1;;2]]];
  upb=1.0*Max[boundlist[[1;;2]]];
  hard$lwb=-Infinity;
  hard$upb=Infinity;
  If[Length[boundlist]>=3,
   If[boundlist[[3]]>upb, hard$upb=boundlist[[3]]];
   If[boundlist[[3]]<lwb, hard$lwb=boundlist[[3]]];
  ];
  If[Length[boundlist]>=4,
   If[boundlist[[4]]>upb, hard$upb=boundlist[[4]]];
   If[boundlist[[4]]<lwb, hard$lwb=boundlist[[4]]];
  ];
  step=upb-lwb;
  Klwb=fn[lwb];
  Kupb=fn[upb];
  last$x=upb;
  If[verbose, Print["CrudeFindRoot: (",lwb," ",Klwb,";   ",upb,",",Kupb,")"] ];
  If[ Klwb*Kupb>0,
   dirn = Sign[Abs[Klwb]-Abs[Kupb]]; (* +1 means go up, -1 means go down *)
   If[dirn==0,
    Print["CrudeFindRoot: fn is flat!"];
    Return[0];
   ];
   If[verbose,
    Print["CrudeFindRoot: ",
    If[dirn>0,"Heading up.","Heading down."] ]
   ];
   While[ Klwb*Kupb>0,
    If[dirn>0, (* heading up *)
     If[ Abs[Klwb]<Abs[Kupb],
      Print["CrudeFindRoot: heading up, sign of slope went positive"];
      Print["(",lwb," ",Klwb,";   ",upb,",",Kupb,")"];
      Return[0];
     ];
     lwb=upb; Klwb=Kupb;
     If[upb+step<hard$upb, upb=upb+step, upb=(upb+hard$upb)/2];
     Kupb=fn[upb];  last$x=upb;
    ,(* else, heading down *)
     If[ Abs[Klwb]>Abs[Kupb],
      Print["CrudeFindRoot: heading down, sign of slope went negative!"];
      Print["(",lwb," ",Klwb,";   ",upb,",",Kupb,")"];
      Return[0];
     ];
     upb=lwb; Kupb=Klwb;
     If[lwb-step>hard$lwb, lwb=lwb-step, lwb=(lwb+hard$lwb)/2];
     Klwb=fn[lwb];  last$x=lwb;
    ];
   If[verbose,Print["CrudeFindRoot: (",lwb," ",Klwb,";   ",upb,",",Kupb,")"] ];
   ];
  ];
  (* Now we have bracketed it *)
  While[ Abs[(upb-lwb)] > accuracy ,
   mid= (lwb+upb)/2.0;
   Kmid=fn[mid]; last$x=mid;
   If[Kmid*Kupb>0,
    upb=mid; Kupb=Kmid;
   ,(*else*)
    lwb=mid; Klwb=Kmid;
   ];
  If[verbose, Print["CrudeFindRoot: (",lwb," ",Klwb,";   ",upb,",",Kupb,")"] ];
  ];
 (* Return the last value of x at which we evaluated the fn.
    This is so that the result is consistent with side-effects of the last
    evaluation *)
 last$x
];

(* ----
f[x_]= (x-20)^2+1;
CrudeFindRoot[f,{1,4}]
CrudeFindRoot[f,{30,32}]

f[x_]= 1/((x-1)*(5-x))-2;  blows up at x=1 and 5!
CrudeFindRoot[f,{2,3,1,5},0.0001,True]

---- *)


CrudeFindMax[fn_,boundlist_,x$accuracy_:0.001,y$accuracy_:0.001,
 verbosity_:0]:=Module[
 {x1,x2,x3,fn$1,fn$2,fn$3,step,x$tol,y$tol,x15,fn$15,x25,fn$25,
  hard$lwb,hard$upb},
(* Find a maximum of the function in the specified range,
   eg CrudeFindMax[Sin,{0,Pi}] 
   boundlist: {lwb,upb} or {lwb,upb,hardbound} or {lwb,upb,hardbound,hardbound}
   {lwb,upb} need not straddle the root; it shifts them in the
   appropriate direction in search of the max.
   The hard bounds cannot be crossed: the search gives up if it reaches them *)
 x$tol=Abs[x$accuracy];
 y$tol=Abs[y$accuracy];
 If[x$tol==0 && y$tol==0, 
  Print["CrudeFindMax: zero tolerances requested!"];
  Abort[];
 ];
 If[x$tol==0, x$tol=Infinity];
 If[y$tol==0, y$tol=Infinity];
 x1=1.0*Min[boundlist[[1;;2]]];
 x3=1.0*Max[boundlist[[1;;2]]];
 hard$lwb=-Infinity;
 hard$upb=Infinity;
 If[Length[boundlist]>=3,
  If[boundlist[[3]]>x3, hard$upb=boundlist[[3]]];
  If[boundlist[[3]]<x1, hard$lwb=boundlist[[3]]];
 ];
 If[Length[boundlist]>=4,
  If[boundlist[[4]]>x3, hard$upb=boundlist[[4]]];
  If[boundlist[[4]]<x1, hard$lwb=boundlist[[4]]];
 ]; 
 x2 = (x1+x3)/2.0; 
 step=x2-x1;
 fn$1= fn[x1];
 fn$2= fn[x2];
 fn$3= fn[x3];
 If[verbosity>0,
  Print["CrudeFindMax: (",x1,", ",x2,", ",x3,"):     ", 
  fn$1,", ",fn$2,", ",fn$3]
 ];
 While[ ! fn$2>Max[fn$1,fn$3] && x1>=hard$lwb && x3<=hard$upb,
  If[fn$2 < Min[fn$1,fn$3],
   Print["CrudeFindMax: we have a minimum instead!"];
   Abort[];
  ];
  If[ fn$3>fn$1 && fn$2>fn$1 && fn$2<fn$3, (* gotta head right *)
   x1=x2; x2=x3; x3=x3+step;
   fn$1=fn$2; fn$2=fn$3; fn$3=fn[x3];
  ,(*else*)
   If[ fn$3<fn$1 && fn$2>fn$3 && fn$2<fn$1, (* gotta head left *)
    x3=x2; x2=x1; x1=x1-step;
    fn$3=fn$2; fn$2=fn$1; fn$1=fn[x1];
   ];
  ];
 ];
 (* If we went out of bounds, give up now: *)
 If[x1<hard$lwb, Return[{x1,-Infinity}]];
 If[x3>hard$upb, Return[{x3,-Infinity}]];
 (* We should have the maximum bracketed. Zoom in on it: *)
 While[!( x3-x1<x$tol && Max[Abs[fn$3-fn$2],Abs[fn$1-fn$2]]<y$tol),
  x15=(x2+x1)/2;
  step = x15-x1;
  fn$15=fn[x15];
  If[fn$15>=fn$2,
   x3=x2; fn$3=fn$2; x2=x15; fn$2=fn$15;
  ,(*else*)
   x25=(x2+x3)/2;
   fn$25=fn[x25];
   If[fn$25>=fn$2,
    x1=x2; fn$1=fn$2; x2=x25; fn$2=fn$25;
   ,(*else*)
    If[fn$2>=fn$15 && fn$2>=fn$25,
     x1=x15; fn$1=fn$15; x3=x25; fn$3=fn$25;
    ,(*else*)
     (* test for perfect flatness? *)
     Print["CrudeFindMax: impossible happened!"];
     Print["x1 = ",x1, " fn = ",fn$1];
     Print["x15= ",x15," fn = ",fn$15];
     Print["x2 = ",x2, " fn = ",fn$2];
     Print["x25= ",x25, "fn = ",fn$25];
     Print["x3 = ",x3, " fn = ",fn$3];
     Return[{False,False}];
     ]
    ]
   ];
  If[verbosity>0,
   Print["CrudeFindMax: (",x1,", ",x2,", ",x3,"):     ", 
    fn$1,", ",fn$2,", ",fn$3,"  ",
    If[fn$2>=Max[fn$1,fn$3],"Bracketed","NOT BRACKETED"]
   ]
  ];
 ];
 {x2,fn$2}
];

(* ----
Needs["MGALibrary`"];
f[x_]= 20-(x-5)^2;
CrudeFindMax[f,{1,4}]      {5.00012, 20.}
CrudeFindMax[f,{1,4,4.5},0.001,0.001,1]   {5.5, -Infinity}
"-Infinity as fn value means that seach failed, in this case because the "
<>"hard bound stopped us from getting to a maximum";
CrudeFindMax[f,{6,9}]    {4.99988, 20.}
CrudeFindMax[f,{6,9,5.5},0.001,0.001,1]   {4.5, -Infinity}
CrudeFindMax[f,{6,9,5.5,10},0.001,0.001,1]  {4.5, -Infinity}

---- *)


CrudePlot[fn_,range_,npts_:20,plotargs_:(Joined -> True)]:=
 Module[{procname,i,x,fnx,return$val$length},
 (* evaluates function at requested number of points and makes a plot.
    If fn returns a list, all those points are plotted *)
 procname="CrudePlot";
 CrudePlot$list={};
  For[i=0,i<=npts,i++,
  x = range[[1]]+(range[[2]]-range[[1]])*i/npts;
  (*Print[x];*)
  fnx = fn[x];
  If[i==0, return$val$length=Length[fnx]];
  If[Length[fnx] != return$val$length,
   Print[procname<>": function returned a list of length ",Length[fnx],
    "unlike previous length ",return$val$length," !!"];
  ];
  If[Length[fnx]<2, 
   CrudePlot$list=Append[CrudePlot$list,{x,fnx}] 
  ,(* else *)
   fnx = Sort[Map[Re,fnx]];  (* You don't always want to do this! *)
   CrudePlot$list=Append[CrudePlot$list,Table[{x,fnx[[j]]},{j,1,Length[fnx]}]];
  ];
 ];
 If[return$val$length<2,
  ListPlot[CrudePlot$list,plotargs ]
 ,(*else*)
  MultiListPlot[Transpose[CrudePlot$list],plotargs]
 ]
];

MultiCrudePlot[fn$list_,range_,npts_:20,verbose_:False,plotargs_:(Joined -> True)] :=Module[{i,j,x},
 (* evaluates each function at requested number of points and 
    plot them all on one plot *)
 MultiCrudePlot$lists=Table[{},{Length[fn$list]}];
 For[i=0,i<=npts,i++,
  For[j=1,j<=Length[fn$list],j++,
   x = range[[1]]+(range[[2]]-range[[1]])*i/(1.0*npts);
   MultiCrudePlot$lists[[j]]=Append[
    MultiCrudePlot$lists[[j]],{x,fn$list[[j]][x]}];
  ];
 ];
 If[verbose,Print[InputForm[MultiCrudePlot$lists]]];
 MultiListPlot[MultiCrudePlot$lists,plotargs ]
];

CumulativeCrudePlot[fn_,range_,npts_:20]:=Module[{i,j,x},
(* keeps a list of previous evaluations, and adds to it,
   without re-calculating the same point twice *)
 If[!ListQ[CCPList],
  Print["Initializing CCPList to empty list"];
  CCPList={};
 ,(*else*)
  Print["Using existing CCPList with ",Length[CCPList]," elements"];
 ];
 For[i=0,i<=npts,i++, (* loop through requested values *)
  x = range[[1]]+(range[[2]]-range[[1]])*i/(1.0*npts);
  found=False;j=0; 
  While[j<Length[CCPList] && ! found, (* see if we already did this one *)
   j=j+1;
   found= (Abs[CCPList[[j]][[1]]-x]<1*^-6);
  ];
  If[ !found,
   CCPList=Append[CCPList,{x,fn[x]}];
   Print[{x,fn[x]}];
  ];
 ];
 CCPList=Sort[CCPList]; (* sorts by value of x *)
 ListPlot[CCPList,Joined -> True ]
];

MultiListPlot[list$of$lists_,plotargs_:(Joined->True)]:=Module[{},
  lp[data_]:=ListPlot[data,plotargs, DisplayFunction->Identity];
  plotlist=Map[lp,list$of$lists];
  Show[plotlist,DisplayFunction->$DisplayFunction]
];


(* Mathematica doesn't know about physicists use of "adjoint": *)
Adjoint[matrix_] := Conjugate[Transpose[matrix]];

(* ++++
Norm[matrix_] := Sum[ Sum[ Conjugate[matrix[[i,j]]]*matrix[[i,j]],
  {j,1,Length[matrix[[1]]]}], {i,1,Length[matrix]}];
++++ *)


Commutator[a_,b_]:= a.b - b.a;
Anticommutator[a_,b_]:= a.b + b.a;

OuterProduct[mata_,matb_]:=Outer[Times,mata,matb];

Orthonormalize[matrix_]:=Module[{locmat,dotprod,tempvec,n,i,j},
(* Useful for making eigenvectors orthonormal, eg
     U = Orthonormalize[Eigenvectors[M]];
     U.M.Inverse[U] is now diagonal
   check: Inverse[U]=Transpose[U] if U is real.
   M. Alford   Sept 1998
*)
(* First normalize *)
 locmat=matrix;
 For[i=1,i<=Length[matrix],i++,
  n = Sqrt[Conjugate[matrix[[i]]].matrix[[i]]];
  locmat[[i]] = matrix[[i]]/n;
 ];
(* Now orthogonalize *)
 For[i=2,i<=Length[locmat],i++,
  tempvec = locmat[[i]];
  For[j=1,j<i,j++,
   dotprod = Conjugate[locmat[[i]]].locmat[[j]];
   tempvec = tempvec - dotprod locmat[[j]];
  ];
  locmat[[i]]=tempvec
 ];
(* Renormalize *)
 For[i=1,i<=Length[locmat],i++,
  n = Sqrt[Conjugate[locmat[[i]]].locmat[[i]]];
  locmat[[i]] = locmat[[i]]/n;
 ];
 locmat
];

TensorProduct[mata_,matb_]:=Module[
 {nrowa,ncola,nrowb,ncolb,nrow,ncol,i,j,ai,aj,bi,bj,resl,crap},
(* Tensor prod of 2 matrices, assumed rectangular 
   M. Alford  Oct 1998 *)
 nrowa = Length[mata];
 ncola = Length[mata[[1]]];
 nrowb = Length[matb];
 ncolb = Length[matb[[1]]];
 nrow = nrowa*nrowb;
 ncol = ncola*ncolb;
 resl=Array[crap,{nrow,ncol}];
 For[i=1,i<=nrow,i++,
  For[j=1,j<=ncol,j++,
   bi = Mod[i,nrowb]; If[bi==0,bi=nrowb];
   bj = Mod[j,ncolb]; If[bj==0,bj=ncolb];
   ai = (i - bi)/nrowb + 1; 
   aj = (j-bj)/ncolb + 1;
(*   Print[i,",",j," -> a[",ai,",",aj,"] b[",bi,",",bj,"]"]; *)
   resl[[i,j]] = mata[[ai]][[aj]] * matb[[bi]][[bj]];
  ];
 ];
 resl
];

TProd := TensorProduct;

(* Check:
Ml1 = Array[ml1,{2,2}];
Ml2 = Array[ml2,{2,2}];
Mr1 = Array[mr1,{2,2}];
Mr2 = Array[mr2,{2,2}];
prod1 = TensorProduct[Ml1,Ml2].TensorProduct[Mr1,Mr2];
prod2 = TensorProduct[Ml1.Mr1, Ml2.Mr2];
Simplify[prod1-prod2]
*)

Hackup[matrix_,nr_Integer,nc_Integer]:=Module[
 {NR,NC,RowPerBlock,ColPerBlock,crap,crud,block,i,j,iblock,jblock,ix,jx,resl},
 (* break up a matrix into nr*nc sub-matrices
    M. Alford Oct 1998 *)
 NR = Length[matrix];
 NC = Length[matrix[[1]]];
 If[ ! (Mod[NR,nr] == 0),
  Print["Number of rows is not divisible by ",nr]; Return[Void];
 ];
 If[ ! (Mod[NC,nc] == 0),
  Print["Number of cols is not divisible by ",nc]; Return[Void];
 ];
 RowPerBlock = NR/nr;
 ColPerBlock = NC/nc;
 resl = Array[crud,{nr,nc}];
 For[iblock=1,iblock<=nr,iblock++,
  For[jblock=1,jblock<=nc,jblock++,
   block = Array[crap,{RowPerBlock,ColPerBlock}];
   For[ix=1,ix<=RowPerBlock,ix++,
    For[jx=1,jx<=ColPerBlock,jx++,
     block[[ix,jx]] = (
       matrix[[ (iblock-1)*RowPerBlock + ix,(jblock-1)*ColPerBlock + jx]] );
    ]
   ];
   resl[[iblock,jblock]]=block;
  ]
 ];
 resl
];

UnHackup[matrix_]:=Module[
 {NR,NC,nr,nc,crap,i,j,iblock,jblock,ix,jx,resl},
 (* Make a hacked matrix go back to a regular matrix 
    M. Alford Nov 2000 *)
 (* size of hacked matrix: *)
 NR = Length[matrix];
 NC = Length[matrix[[1]]];
 (* size of each cell: *)
 nr = Length[matrix[[1,1]]];
 nc = Length[matrix[[1,1]][[1]]];
 resl = Array[crud,{NR*nr,NC*nc}];
 i=1; j=0;
 For[iblock=1,iblock<=NR,iblock++,
  For[jblock=1,jblock<=NC,jblock++,
   For[ix=1,ix<=nr,ix++,
    For[jx=1,jx<=nc,jx++,
     i = (iblock-1)*nr + ix;
     j = (jblock-1)*nc + jx;
     resl[[i,j]]=matrix[[iblock,jblock]][[ix,jx]]
    ]
   ]
  ]
 ];
 resl
];

(* Convert float x to a string with no more than n decimal places *)
OldRoundFloat[x_,n_]:=Module[{procname,i,s,pairlist,crap,decimal,end,ndp},
 procname="RoundFloat";
 s = ToString[x,FormatType -> InputForm];
 If[n<0,
  Print[procname<>": no of decimal places cannot be negative!"];
  Return[s];
 ];
 If[! NumberQ[x],
  Print[procname<>": first argument must be a number!"];
  Return[s];
 ];
 pairlist=StringPosition[s,"."];
 If[Length[pairlist]==0,Return[s]];
 If[Length[pairlist]>1, 
  Print[procname<>": multiple decimal points in "<>s];
  Return[s];
 ]; 
 {decimal,crap}=pairlist[[1]];
 If[decimal!=crap,
  Print[procname<>": impossible happened: decimal point position is ",
   pairlist[[1]]];
  Return[s];
 ];
 end=decimal+1;
 While[end<=StringLength[s] && DigitQ[StringTake[s,{decimal+1,end}]],
  end+=1
 ];
 ndp=end-1-decimal;
 If[ndp<=n, Return[s],
  Return[StringDrop[s, {decimal+1+n,end-1}]]
 ];
];


End[];  (* End MGALibrary`Private` *)

EndPackage[];
