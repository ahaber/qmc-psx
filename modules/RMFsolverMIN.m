(* ::Package:: *)

(* ::Title:: *)
(*RMF Solver Package*)


(* ::Input::Initialization:: *)
(*Solver for general RMF for npe-matter in beta equilibrium and charge neutrality, pure neutron matter (PNM), or symmetric nuclear matter (SYM), T=0 and finite T included,  written by Alexander Haber, minimal version needed for QMC-PSX project May 2022*)

BeginPackage["RMFsolverMIN`"];
(*public functions*)
{RMFsolve,proton$fraction$RMF,RMFpressure,electron$pressure,n$electron,nucleon$pressure,RMFsolveSYM,RMFpressureSYM,edens$RMF,pressure$RMF,binding$energy$RMF,pressure$RMF$PNM,RMFpressurePNM,RMFsolvePNM,RMFbindingPNM,RMFsolve$nb,baryon$density,RMFbindingSYM,RMFsolve$nb$PNM,RMFsolve$nb$SYM,create$EOS,DeltaMU,dU$threshold}

Begin["`Private`"]

(*baryon density for interacting baryons*)
nB[T_?NumericQ,\[Mu]B_?NumericQ,BI_?NumericQ,\[Sigma]_?NumericQ,w0_?NumericQ,r03_?NumericQ]:=If[T>= 1,1/\[Pi]^2*NIntegrate[k^2/(1+Exp[(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2]-(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03))/T])-k^2/(1+Exp[(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2]+(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*bar*r03))/T]),{k,0,upB}],Sqrt[(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03)^2-(mB[[BI]]-gs[[BI]]*\[Sigma])^2]^3/(3\[Pi]^2)];

(*baryon density for interacting baryons - public function that sets parameters as well*)
baryon$density[T_?NumericQ,\[Mu]B_?NumericQ,BI_?NumericQ,\[Sigma]_?NumericQ,w0_?NumericQ,r03_?NumericQ,para_]:=Module[{dens},
set$couplings[para];
dens=nB[T,\[Mu]B,BI,\[Sigma],w0,r03];
Return[dens]
];

(*baryon scalar density, source term for sigma meson*)
ns[T_?NumericQ,\[Mu]B_?NumericQ,BI_?NumericQ,\[Sigma]_?NumericQ,w0_?NumericQ,r03_?NumericQ]:=If[T>=1,1/Pi^2(NIntegrate[(mB[[BI]]-gs[[BI]]*\[Sigma])/Sqrt[(k^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2)]k^2*(1/(1+Exp[(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2]-(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03))/T])+1/(1+Exp[(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2]+(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*bar*I3[[BI]]*r03))/T])),{k,0,upB}]),(mB[[BI]]-gs[[BI]]*\[Sigma])/(2\[Pi]^2)(Sqrt[(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03)^2-(mB[[BI]]-gs[[BI]]*\[Sigma])^2]*((\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03))-(mB[[BI]]-gs[[BI]]*\[Sigma])^2Log[(Sqrt[(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03)^2-(mB[[BI]]-gs[[BI]]*\[Sigma])^2]+(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03))/(mB[[BI]]-gs[[BI]]*\[Sigma])])];

(*free fermion density*)
n$electron[T_?NumericQ,\[Mu]Q_?NumericQ]:=If[T>= 1,1/\[Pi]^2*NIntegrate[k^2/(1+Exp[(Sqrt[k^2+me^2]-\[Mu]Q)/T]),{k,0,upB}]-1/\[Pi]^2*NIntegrate[k^2/(1+Exp[(Sqrt[k^2+me^2]+\[Mu]Q)/T]),{k,0,upB}],1/(3\[Pi]^2)*Sqrt[\[Mu]Q^2-me^2]^3];
kF$electron[muQ_?NumericQ]:=CubeRoot[3*\[Pi]^2*n$electron[0,muQ]];
(*Lagrangian of SFH0 RMF (see compose for couplings) https://compose.obspm.fr/eos/34, Lagrangian in nucl-th/0410066,
*)
fswr[\[Sigma]_,w0_]:=(b1*w0^2+b2*w0^4+b3*w0^6+a1*\[Sigma]+a2*\[Sigma]^2+a3*\[Sigma]^3+a4*\[Sigma]^4+a5*\[Sigma]^5+a6*\[Sigma]^6);
U$sigma[s_]:=1/2*ms^2*s^2+b*Mn/3(gsn*s)^3+c/4(gsn*s)^4;
Lmes[\[Sigma]_,w0_,r03_]:= -U$sigma[\[Sigma]]+1/2*mw^2*w0^2+zet/24*gw[[1]]^4*w0^4+1/2*mr^2*r03^2+xi/24*gr[[1]]^4*r03^4+gr[[1]]^2*fswr[\[Sigma],w0]r03^2;

(*Fermi momentum for baryons in RMF, needs set$couplings*)
kf$bar[\[Sigma]_,w0_,r03_,\[Mu]B_,BI_]:=Sqrt[(\[Mu]B-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03)^2-(mB[[BI]]-gs[[BI]]*\[Sigma])^2];

(*E_F^* for baryons = Sqrt[kf^2+m*^2] (no vector mean fields), needs set$couplings*)
Ef$bar[\[Sigma]_,w0_,r03_,\[Mu]B_,BI_]:=Sqrt[kf$bar[\[Sigma],w0,r03,\[Mu]B,BI]^2+(mB[[BI]]-gs[[BI]]*\[Sigma])^2];

(*fermion pressure for free fermion, needs set$couplings*)
electron$pressure[T_,muq_]:=If[T==0,1/(8\[Pi]^2)(2/3*muq^4),NIntegrate[2*T*k^2*4\[Pi]/(2\[Pi])^3*(Log[1+Exp[-(Sqrt[k^2+me^2]-muq)/T]]+Log[1+Exp[-(Sqrt[k^2+me^2]+muq)/T]]),{k,0,upB}]];

(*fermion pressure for baryons, needs set$couplings*)
nucleon$pressure[T_,mu_,sig_,w0_,r03_,BI_]:=If[T<= 1,1/(8\[Pi]^2)*((2/3kf$bar[sig,w0,r03,mu,BI]^3-(mB[[BI]]-gs[[BI]]*sig)^2kf$bar[sig,w0,r03,mu,BI])Ef$bar[sig,w0,r03,mu,BI]+(mB[[BI]]-gs[[BI]]*sig)^4*(Log[(kf$bar[sig,w0,r03,mu,BI]+Ef$bar[sig,w0,r03,mu,BI])/((mB[[BI]]-gs[[BI]]*sig))])),2*T*NIntegrate[4\[Pi]/(2\[Pi])^3k^2(Log[1+Exp[-(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*sig)^2]-(mu-gw[[BI]]*w0-gr[[BI]]*I3[[BI]]*r03))/Trmf]]+Log[1+Exp[-(Sqrt[k^2+(mB[[BI]]-gs[[BI]]*sig)^2]+(mu-gw[[BI]]*w0-bar*gr[[BI]]*I3[[BI]]*r03))/Trmf]]),{k,0,upB}]];
(*energy density of one specific baryon species in RMF , needs set$couplings*)
nucleon$edens[T_,mu_,sig_,w0_,r03_,BI_]:=nucleon$pressure[T,mu,sig,w0,r03,BI]-mu*nB[T,mu,BI,sig,w0,r03];

(*Module to set couplings, me and upper bound for NIntegrate FIXED!*)
(*para={{mn,mp},{m$sigma,m$omega,m$rho},{I3n,I3p},{gsn,gsp},{gwn,gwp},{grn,grp},{b,c,Mn$scale},{omega4$coupling},{rho4$coupling},{b1,b2,b3,a1,a2,a3,a4,a5,a6},nsat,bar};*)
set$couplings[para_]:=Module[{},
ClearAll[ms,mw,mr,b,c,Mn,mB,gsn,grn,gwn,gsp,gwp,grp,gs,gw,gr,I3,I3n,I3p,zet,xi,b1,b2,b3,a1,a2,a3,a4,a5,a6,me,upB];
ms=para[[2,1]];
mw=para[[2,2]];
mr=para[[2,3]];
b=para[[7,1]];
c=para[[7,2]];
Mn=para[[7,3]];
mB=para[[1]];
gsn=para[[4,1]];
gs=para[[4]];
gw=para[[5]];
gr=para[[6]];
I3=para[[3]];
zet=para[[8,1]];
xi=para[[9,1]];bar=-1;
b1=para[[10,1]];b2=para[[10,2]];b3=para[[10,3]];a1=para[[10,4]];a2=para[[10,5]];a3=para[[10,6]];a4=para[[10,7]];a5=para[[10,8]];a6=para[[10,9]];me=0.511;upB=5000;]

(* Beta Equilibrated npe matter , charge neutrality imposed*)

(*RMF solving module, inputs are baryon density, temperature, coupling constants for the RMF model in para and initial guesses. For T<1MeV a T=0 solver is used, for higher T the full T dependence is taken into account. Output is in form of replacement rules + checks + T+ RMFmodel couplings *)

RMFsolve[nbext_?NumericQ,Trmf_?NumericQ,para_,\[Sigma]s_?NumericQ,w0s_?NumericQ,r03s_?NumericQ,\[Mu]bs_?NumericQ,\[Mu]es_?NumericQ,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,Eq5,s,w,rr,\[Sigma],w0,r03,\[Mu]b,\[Mu]e},
set$couplings[para];
If[verb==True,If[Trmf<= 1,Print["Temperature is either 0 or T< 1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];

Eq1[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=-(gs[[1]]*ns[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gs[[2]]*ns[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};
Eq2[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=-(gw[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq4[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]+nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03]-nbext;
Eq5[\[Sigma]_,w0_,r03_,\[Mu]b_,\[Mu]e_]:=n$electron[Trmf,\[Mu]e]-nB[Trmf,\[Mu]b-\[Mu]e,2,\[Sigma],w0,r03];
reslist=FindRoot[{Eq1[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]==0,Eq2[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]==0,Eq3[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]==0,Eq4[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]==0,Eq5[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{r03,Re[r03s]},{\[Mu]e,Re[\[Mu]es]},{\[Mu]b,Re[\[Mu]bs]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,r03/.reslist,\[Mu]b/.reslist,(\[Mu]b-\[Mu]e)/.reslist},{(Eq1[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]/.reslist),(Eq2[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]/.reslist),Eq3[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]/.reslist,Eq4[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]/.reslist,Eq5[\[Sigma],w0,r03,\[Mu]b,\[Mu]e]/.reslist},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]


]
(*module to solve RMF for various densities and automatically updating the initial guesses*)
RMFsolve$nb[nbstart_,nbeend_,nbpoints_,T_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,\[Mu]es_,verb_?BooleanQ]:=Module[{nbrestab,ss,ws,rs,mus,mues,nbs,nbe,nbp,dnb,nsat},
ss=\[Sigma]s;ws=w0s;rs=r03s;mus=\[Mu]bs;mues=\[Mu]es;nbrestab={};nbs=nbstart;nbe=nbeend;nbp=nbpoints;dnb=(nbe-nbs)/nbp;
Do[((*Print[nbs+(i-1)dnb];*)
nbrestab=Append[nbrestab,{nbs+(i-1)dnb,Quiet[RMFsolve[(nbs+(i-1)dnb),T,para,ss,ws,rs,mus,mues,verb]]}];
ss=Re[nbrestab[[i,2,1,1]]];
ws=Re[nbrestab[[i,2,1,2]]];
rs=Re[nbrestab[[i,2,1,3]]];
mus=Re[nbrestab[[i,2,1,4]]];
mues=Abs[Re[nbrestab[[i,2,1,4]]]-Re[nbrestab[[i,2,1,5]]]];

)
,{i,1,nbp+1,1}];
Return[nbrestab]
]

(*module to solve RMF AND compute pressure for beta-equilibrated charge neutral npe matter*)
RMFpressure[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,\[Mu]es_,verb_?BooleanQ]:=Module[{meson$pressure,nucpress,vevs,sig,w0,r03,mub,mue,k,BI,muv,chk},
vevs=RMFsolve[nbext,Trmf,para,\[Sigma]s,w0s,r03s,\[Mu]bs,\[Mu]es,verb];
chk=vevs[[2]];
sig=vevs[[1,1]];
w0=vevs[[1,2]];
r03=vevs[[1,3]];
mub=vevs[[1,4]];
mue=vevs[[1,4]]-vevs[[1,5]];
muv={mub,mub-mue};
meson$pressure=Lmes[sig,w0,r03];
nucpress=Sum[nucleon$pressure[Trmf,muv[[BI]],sig,w0,r03,BI],{BI,1,2,1}];
(*nucpress=nucleon$pressure[muv[[1]],sig,w0,r03,1]+nucleon$pressure[muv[[2]],sig,w0,r03,2]*);

If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[meson$pressure+nucpress+electron$pressure[Trmf,mue]]]

]

(*module to compute pressure for given solution of RMF obtained by RMFsolver*)
(*soltab is input created directly from RMVsolve,RMVsolvePNM or RMVsolveSYM*)
pressure$RMF[soltab_]:=Module[{T,para,sig,w0,r03,mun,mup,muv,mue,meson$pressure,nucpress},
para=soltab[[5]];
set$couplings[para];
T=soltab[[3]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=soltab[[1,5]];
mue=If[mup>0,mun-mup,0]; (*this is sketchy, it only works for PNM,SYM, and charge neutral beta equilibrated matter, but might fail in more general settings!*)
muv={mun,mup};
meson$pressure=Lmes[sig,w0,r03];
nucpress=If[mup>0,Sum[nucleon$pressure[T,muv[[BI]],sig,w0,r03,BI],{BI,1,2,1}],nucleon$pressure[T,mun,sig,w0,r03,1]];
Return[meson$pressure+nucpress+electron$pressure[T,mue]];
]
(*module to compute edensity for given solution of RMF obtained by RMFsolver*)
(*soltab is input created directly from RMVsolve,RMVsolvePNM or RMVsolveSYM*)
edens$RMF[soltab_]:=Module[{T,para,sig,w0,r03,mun,mup,totpress,edens,mue},
para=soltab[[5]];
set$couplings[para];
T=soltab[[3]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=soltab[[1,5]];
mue=If[mup>0,mun-mup,0]; (*this is sketchy, it only works for PNM,SYM, and charge neutral beta equilibrated matter, but might fail in more general settings!*)
muv={mun,mup};
totpress=pressure$RMF[soltab];
edens=-(totpress-mun*nB[T,mun,1,sig,w0,r03]-mup*nB[T,mup,2,sig,w0,r03]-(mue)*n$electron[T,mue]);
Return[edens]

]
(*module to compute EOS P(eps) for given solution of RMF obtained by RMFsolver*)
(*soltab is input created directly from RMFsolve,RMFsolvePNM or RMFsolveSYM*)
create$EOS[soltab_]:=Module[{T,para,sig,w0,r03,mun,mup,totpress,edens,mue,mub},
para=soltab[[5]];
set$couplings[para];
T=soltab[[3]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=soltab[[1,5]];
mue=If[mup>0,mun-mup,0]; (*this is sketchy, it only works for PNM,SYM, and charge neutral beta equilibrated matter, but might fail in more general settings!*)
muv={mun,mup};
totpress=pressure$RMF[soltab];
edens=-(totpress-mun*nB[T,mun,1,sig,w0,r03]-mup*nB[T,mup,2,sig,w0,r03]-(mue)*n$electron[T,mue]);
Return[{totpress,edens,mun}]

]
(*module to compute binding energy for given solution of RMF obtained by RMFsolver,RMVsolvePNM or RMVsolveSYM*)
(*soltab is input created directly from RMVsolveSYM*)
binding$energy$RMF[soltab_]:=Module[{T,para,sig,w0,r03,mun,mup,totpress,binding,bind,dens},
para=soltab[[5]];
set$couplings[para];
dens=soltab[[4]];
bind=(edens$RMF[soltab]/dens-mB[[1]]);
Return[bind]

]
(*module to compute proton fraction for given solution of RMF obtained by RMFsolver*)
(*soltab is input created directly from RMVsolve*)
proton$fraction$RMF[soltab_]:=Module[{T,para,sig,w0,r03,mun,mup,xp,dens},
para=soltab[[5]];
set$couplings[para];
T=soltab[[3]];
dens=soltab[[4]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=soltab[[1,5]];
xp=nB[T,mup,2,sig,w0,r03]/dens;
Return[xp]

]

(*compute dU threshold in beta equilibrated npe matter, imput is para list*)
dU$threshold[para_]:=Module[{nucdens,soltab$tab,kfntab,kfptab,kfetab,momsurplus,pl,dUthr,x,dUthrout},
nucdens=para[[-2]];
soltab$tab=RMFsolve$nb[0.5*nucdens,10*nucdens,100,0,para,30,20,-3,990,100,False];
kfntab=Table[{soltab$tab[[i,1]],kf$bar[soltab$tab[[i,2,1,1]],soltab$tab[[i,2,1,2]],soltab$tab[[i,2,1,3]],soltab$tab[[i,2,1,4]],1]},{i,1,Length[soltab$tab]}];
kfptab=Table[{soltab$tab[[i,1]],kf$bar[soltab$tab[[i,2,1,1]],soltab$tab[[i,2,1,2]],soltab$tab[[i,2,1,3]],soltab$tab[[i,2,1,5]],2]},{i,1,Length[soltab$tab]}];
kfetab=Table[{soltab$tab[[i,1]],kF$electron[soltab$tab[[i,2,1,4]]-soltab$tab[[i,2,1,5]]]},{i,1,Length[soltab$tab]}];
momsurplus=Table[{kfntab[[i,1]]/nucdens,-kfntab[[i,2]]+kfptab[[i,2]]+kfetab[[i,2]]},{i,1,Length[soltab$tab]}];
pl=ListLinePlot[momsurplus,Axes->False,Frame->True,FrameLabel->{"nB/\!\(\*SubscriptBox[\(n\), \(0\)]\)","kFn-kFp-kFe [MeV]"}];

dUthr=x/.FindRoot[Interpolation[momsurplus,InterpolationOrder->1][x]==0,{x,3}];
If[dUthr>10 || dUthr<0.5,dUthrout="No dU threshold found between 0.5 and 10 n0",dUthrout=dUthr];
Return[{pl,dUthrout}]]

(*SYMMETRIC NUCLEAR MATTER, NO ELECTRONS *)

(*RMF solver for symmetric nuclear matter, no electrons*)
RMFsolveSYM[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,Eq5,s,w,rr,\[Sigma],w0,r03,\[Mu]b},
set$couplings[para];
If[verb==True,If[Trmf<= 1,Print["Temperature is either 0 or T< 1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];

Eq1[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=-(gs[[1]]*ns[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gs[[2]]*ns[Trmf,\[Mu]p,2,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};
Eq2[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=-(gw[[1]]*nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gw[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+gr[[2]]*I3[[2]]*nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq4[\[Sigma]_,w0_,r03_,\[Mu]n_,\[Mu]p_]:=nB[Trmf,\[Mu]n,1,\[Sigma],w0,r03]+nB[Trmf,\[Mu]p,2,\[Sigma],w0,r03]-nbext;
reslist=FindRoot[{Eq1[\[Sigma],w0,0,\[Mu]b,\[Mu]b]==0,Eq2[\[Sigma],w0,0,\[Mu]b,\[Mu]b]==0,Eq4[\[Sigma],w0,0,\[Mu]b,\[Mu]b]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{\[Mu]b,Re[\[Mu]bs]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,0,\[Mu]b/.reslist,\[Mu]b/.reslist},{(Eq1[\[Sigma],w0,0,\[Mu]b,\[Mu]b]/.reslist),(Eq2[\[Sigma],w0,0,\[Mu]b,\[Mu]b]/.reslist),Eq4[\[Sigma],w0,0,\[Mu]b,\[Mu]b]/.reslist},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]


]
(* module to solve SNM RMFs as density table*)
RMFsolve$nb$SYM[nbstart_,nbeend_,nbpoints_,T_,para_,\[Sigma]s_,w0s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{nbrestab,ss,ws,rs,mus,mues,nbs,nbe,nbp,dnb,nsat},
ss=\[Sigma]s;ws=w0s;mus=\[Mu]bs;nbrestab={};nbs=nbstart;nbe=nbeend;nbp=nbpoints;dnb=(nbe-nbs)/nbp;
Do[((*Print[nbs+(i-1)dnb];*)
nbrestab=Append[nbrestab,{nbs+(i-1)dnb,Quiet[RMFsolveSYM[(nbs+(i-1)dnb),T,para,ss,ws,mus,verb]]}];
ss=Re[nbrestab[[i,2,1,1]]];
ws=Re[nbrestab[[i,2,1,2]]];
mus=Re[nbrestab[[i,2,1,4]]];
)
,{i,1,nbp+1,1}];
Return[nbrestab]
]
(*module to solve RMF AND compute pressure for symmetric  np matter*)
RMFpressureSYM[nbext_?NumericQ,Trmf_?NumericQ,para_,\[Sigma]s_?NumericQ,w0s_?NumericQ,\[Mu]bs_?NumericQ,verb_?BooleanQ]:=Module[{meson$pressure,nucpress,vevs,sig,w0,r03,mub,mue,k,BI,muv,chk},
vevs=RMFsolveSYM[nbext,Trmf,para,\[Sigma]s,w0s,\[Mu]bs,verb];
chk=vevs[[2]];
sig=vevs[[1,1]];
w0=vevs[[1,2]];
r03=0;
mub=vevs[[1,4]];
muv={mub,mub};
meson$pressure=Lmes[sig,w0,0];
nucpress=Sum[nucleon$pressure[Trmf,muv[[BI]],sig,w0,0,BI],{BI,1,2,1}];If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[meson$pressure+nucpress]],Return[meson$pressure+nucpress]]

]
(*module to solve RMF AND compute binding energy SNM*)
RMFbindingSYM[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{meson$pressure,nucpress,vevs,sig,w0,r03,k,muv,chk,mup,mun,mub,bind
},
set$couplings[para];
vevs=RMFsolveSYM[nbext,Trmf,para,\[Sigma]s,w0s,\[Mu]bs,verb];
chk=vevs[[2]];
sig=vevs[[1,1]];
w0=vevs[[1,2]];
r03=0;
mub=vevs[[1,4]];
muv={mub,mub};
bind=binding$energy$RMF[vevs];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[bind]],Return[bind]]
]


(*PURE NEUTRON MATTER *)
(*RMF solving module, inputs are baryon density, temperature, coupling constants for the RMF model in para and initial guesses. For T<1MeV a T=0 solver is used, for higher T the full T dependence is taken into account. Output is in form of replacement rules + checks + T+ RMFmodel couplings *)

RMFsolvePNM[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{reslist,Eq1,Eq2,Eq3,Eq4,s,w,rr,\[Sigma],w0,r03,\[Mu]b},
set$couplings[para];
If[verb==True,If[Trmf<= 1,Print["Temperature is either 0 or T< 1MeV, T too small for finite T numerical integration, T=0 RMF solver is used"]]];

Eq1[\[Sigma]_,w0_,r03_,\[Mu]b_]:=-(gs[[1]]*ns[Trmf,\[Mu]b,1,\[Sigma],w0,r03])-D[Lmes[s,w0,r03],s]/.{s->\[Sigma]};
Eq2[\[Sigma]_,w0_,r03_,\[Mu]b_]:=-(gw[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w,r03],w]/.{w->w0};
Eq3[\[Sigma]_,w0_,r03_,\[Mu]b_]:=-(gr[[1]]*I3[[1]]*nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03])+D[Lmes[\[Sigma],w0,rr],rr]/.{rr->r03};
Eq4[\[Sigma]_,w0_,r03_,\[Mu]b_]:=nB[Trmf,\[Mu]b,1,\[Sigma],w0,r03]-nbext;

reslist=FindRoot[{Eq1[\[Sigma],w0,r03,\[Mu]b]==0,Eq2[\[Sigma],w0,r03,\[Mu]b]==0,Eq3[\[Sigma],w0,r03,\[Mu]b]==0,Eq4[\[Sigma],w0,r03,\[Mu]b]==0},{{\[Sigma],Re[\[Sigma]s]},{w0,Re[w0s]},{r03,Re[r03s]},{\[Mu]b,Re[\[Mu]bs]}},MaxIterations->60000,AccuracyGoal->10,PrecisionGoal->10,WorkingPrecision->MachinePrecision];
out={{\[Sigma]/.reslist,w0/.reslist,r03/.reslist,\[Mu]b/.reslist,0},{(Eq1[\[Sigma],w0,r03,\[Mu]b]/.reslist),(Eq2[\[Sigma],w0,r03,\[Mu]b]/.reslist),Eq3[\[Sigma],w0,r03,\[Mu]b]/.reslist,Eq4[\[Sigma],w0,r03,\[Mu]b]/.reslist,0},Trmf,nbext,para};
chk=out[[2]];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[out]],Return[out]]


]
(*module to solve PNM RMFs for density range *)
RMFsolve$nb$PNM[nbstart_,nbeend_,nbpoints_,T_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{nbrestab,ss,ws,rs,mus,mues,nbs,nbe,nbp,dnb,nsat},
ss=\[Sigma]s;ws=w0s;rs=r03s;mus=\[Mu]bs;nbrestab={};nbs=nbstart;nbe=nbeend;nbp=nbpoints;dnb=(nbe-nbs)/nbp;
Do[((*Print[nbs+(i-1)dnb];*)
nbrestab=Append[nbrestab,{nbs+(i-1)dnb,Quiet[RMFsolvePNM[(nbs+(i-1)dnb),T,para,ss,ws,rs,mus,verb]]}];
ss=Re[nbrestab[[i,2,1,1]]];
ws=Re[nbrestab[[i,2,1,2]]];
rs=Re[nbrestab[[i,2,1,3]]];
mus=Re[nbrestab[[i,2,1,4]]];
)
,{i,1,nbp+1,1}];
Return[nbrestab]
]


(*module to solve RMF AND compute pressure PNM*)
RMFpressurePNM[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{meson$pressure,nucpress,vevs,sig,w0,r03,mub,k,muv,chk},
set$couplings[para];
vevs=RMFsolvePNM[nbext,Trmf,para,\[Sigma]s,w0s,r03s,\[Mu]bs,verb];
chk=vevs[[2]];
sig=vevs[[1,1]];
w0=vevs[[1,2]];
r03=vevs[[1,3]];
mub=vevs[[1,4]];
muv={mub,0};
meson$pressure=Lmes[sig,w0,r03];
nucpress=nucleon$pressure[Trmf,muv[[1]],sig,w0,r03,1];

If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[meson$pressure+nucpress]],Return[meson$pressure+nucpress]]

]

(*module to solve RMF AND compute binding energy PNM*)
RMFbindingPNM[nbext_,Trmf_,para_,\[Sigma]s_,w0s_,r03s_,\[Mu]bs_,verb_?BooleanQ]:=Module[{meson$pressure,nucpress,vevs,sig,w0,r03,k,muv,chk,mup,mun,
},
set$couplings[para];
vevs=RMFsolvePNM[nbext,Trmf,para,\[Sigma]s,w0s,r03s,\[Mu]bs,verb];
chk=vevs[[2]];
sig=vevs[[1,1]];
w0=vevs[[1,2]];
r03=vevs[[1,3]];
mun=vevs[[1,4]];
mup=0;
muv={mun,mup};
bind=binding$energy$RMF[vevs];
If[verb==True,If[Max[chk]>10^(-6),Print["Accuracy not achieved, check functions > 10^(-6)"],Return[bind]],Return[bind]]
]

(*module to compute pressure for given solution of RMF obtained by RMFsolver*)
(*soltab is input created directly from RMFsolve*)
pressure$RMF$PNM[soltab_]:=Module[{T,para,sig,w0,r03,mun,mup,muv,meson$pressure,nucpress},
para=soltab[[5]];
set$couplings[para];
T=soltab[[3]];
sig=soltab[[1,1]];
w0=soltab[[1,2]];
r03=soltab[[1,3]];
mun=soltab[[1,4]];
mup=0;
muv={mun,mup};
meson$pressure=Lmes[sig,w0,r03];
nucpress=nucleon$pressure[T,muv[[1]],sig,w0,r03,1];
Return[meson$pressure+nucpress];
]



End[];
EndPackage[];
