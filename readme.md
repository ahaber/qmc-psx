# QMC-RMFx

This code is used to compute the relativistic mean field models QMC-RMF1, QMC-RMF2, QMC-RMF3, and QMC-RMF4 as published in arxiv:2205.10283

For reproducing the fit of the four relativistic mean field models to the QMC pure neutron matter calculation download the Mathematica Notebook "QMC_PSX_fit.nb" and the "modules" , "input_data" and (optional but recommended) "results_data"

For analyzing the four relativistic mean field models QMC-RMF1/2/3/4 and computing the EOS and other quantities, download the Mathematica Notebook "QMC_PSX.nb" and the "modules" , "input_data" and (optional but recommended) "results_data"

For recreating the plots from arxiv:2205.10283, download the Mathematica Notebook "QMC_PSX.nb" and the "modules" , "input_data" and  "results_data"

For computing additional M-R curves and the tidal deformability, download the python files "constants.py", "tov.py" ,  the jupyter notebook "QMC_PSX.ipynb" and the folder "input_data". The script can be uploaded to  https://colab.research.google.com/ together with "constants.py, "tov.py" and the four data files "QMC_PS1.dat", "QMC_PS2.dat" ,"QMC_PS3.dat" , and "QMC_PS4.dat". 

Questions? contact: ahaber@physics.wustl.edu
